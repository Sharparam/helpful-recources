# helpful recources

This is a project containing a bunch of vector and image files in regards of neurodiverisity, autism and mental health.

## Mental Health Status Stickerpack.
Here are all the workfiles and images for a mental health sticker pack, that can be found on telegram.

Link: https://t.me/addstickers/mh_status

## Neurodiversity Pride Flags

Here are a bunch of diffrent versions of a neurodiversity pride flag I've made including autism-specific flags.

## Lisence:
The Lisence to all the things included here is CC0.

*Please note:*

*I have made a mistake and added a modified Mastodon-depiction to this repo once and forgot to check their lisence. The commit has been reverted and I am sorry for having made that mistake.*

*See Mastodons Code-Lisence here: https://github.com/tootsuite/mastodon/blob/master/LICENSE*

*I was not able to find any Information regarding the Lisencing of their Logos anywhere. I am assuming they fall under the same lisence as their code.*

## Disclaimer:
I am only human and therefore flawed. I am also not 100% aware of everything, that some people within LGBTQ+ communities might not feel safe with. If there is something within this repo, that you find negative, please let me know so I can remove or change it.
